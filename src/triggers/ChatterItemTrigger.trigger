trigger ChatterItemTrigger on feeditem (after insert) {
// test triiger deployment
	system.debug('chatter item trigger');
    system.debug('');
	for (feeditem fi:trigger.new){
		
		if (fi.ParentId.getSObjectType() == VisionModel__c.SObjectType){
			Vision.predictChatter(fi.Id);
		} else if (fi.ParentId.getSObjectType() == Label__c.SObjectType){
			//send example to that label
			//Vision.pChatter(fi.Id);
		}
	}

}