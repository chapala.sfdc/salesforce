/**
* This Test class is created to make a
* mock callout to try out Einstein 
* Intent APIs responses.
**/
@isTest
global class FindIntentControllerMockGenerator implements HttpCalloutMock{
    global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        if (request.getEndpoint().endsWith('sentiment')){
            
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"probabilities": [{"label": "positive",'+
                             '"probability": 0.8673582'+
                             '},{"label": "negative","probability": 0.1316828'+
                             ' },{"label": "neutral",'+
                             '"probability": 0.0009590242'+
                             '  }], "object": "predictresponse"}');
            response.setStatusCode(200);
            
        }
        else if (request.getEndpoint().endsWith('token')){         
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"access_token": 100sf4804}');
            response.setStatusCode(200);
            
        }
        
        else if(request.getEndpoint().endsWith('upload')){
        response.setHeader('Content-Type', 'application/json');
            response.setBody('{"id": 1004804,'+
                             '"name": "case_routing_intent.csv",'+
                             '"createdAt": "2017-06-22T19:31:58.000+0000.",'+
                             '"updatedAt": "2017-06-22T19:31:58.000+0000",'+
                             '"labelSummary": {'+
                             '"labels": []'+
                             '},'+
                             '"totalExamples": 0,'+
                             '"available": false,'+
                             '"statusMsg": "UPLOADING",'+
                             '"type": "text-intent",'+
                             '"object": "dataset"'+
                             '}');
            response.setStatusCode(200);
        }
        
    
    else if(request.getEndpoint().endsWith('12345')){
         response.setHeader('Content-Type', 'application/json');
            response.setBody('{"id": 1004804,"name": "case_routing_intent.csv",'+
                             '"createdAt": "2017-06-22T19:31:58.000+0000",'+
                             '"updatedAt": "2017-06-22T19:31:59.000+0000",'+
                             '"labelSummary": {'+
                             '"labels": ['+
                             '{'+
                             '"id": 23649,'+
                             '"datasetId": 1004804,'+
                             '"name": "Order Change",'+
                             '"numExamples": 26'+
                             '},'+
                             '{'+
                             '"id": 23650,'+
                             '"datasetId": 1004804,'+
                             '"name": "Sales Opportunity",'+
                             '"numExamples": 44'+
                             '},'+
                             '{'+
                             '"id": 23651,'+
                             '"datasetId": 1004804,'+
                             '"name": "Billing",'+
                             '"numExamples": 24'+
                             '},'+
                             '{'+
                             '"id": 23652,'+
                             '"datasetId": 1004804,'+
                             '"name": "Shipping Info","numExamples": 30},'+
                             '{'+
                             '"id": 23653,'+
                             '"datasetId": 1004804,'+
                             '"name": "Password Help",'+
                             '"numExamples": 26}]'+
                             '},'+
                             '"totalExamples": 150,'+
                             '"totalLabels": 5,'+
                             '"available": true,'+
                             '"statusMsg": "SUCCEEDED",'+
                             '"type": "text-intent",'+
                             ' "object": "dataset"}');
            response.setStatusCode(200);

    }
          else if(request.getEndpoint().endsWith('train')){
        response.setHeader('Content-Type', 'application/json');
            response.setBody('{"datasetId": 1004804,'+
                             '"datasetVersionId": 0,'+
                             '"name": "Case Routing Model",'+
                             '"status": "QUEUED",'+
                             '"progress": 0,'+
                             '"createdAt": "2017-06-22T19:39:38.000+0000",'+
                             '"updatedAt": "2017-06-22T19:39:38.000+0000",'+
                             '"learningRate": 0,'+
                             '"epochs": 0,'+
                             '"queuePosition": 1,'+
                             '"object": "training",'+
                             '"modelId": "5SXGNLCCOFGTMNMQYEOTAGBPVU",'+
                             '"trainParams": null,'+
                             '"trainStats": null,'+
                             '"modelType": "text-intent"}');
            response.setStatusCode(200);
        }
        
         else if(request.getEndpoint().endsWith('678910')){
        response.setHeader('Content-Type', 'application/json');
            response.setBody('{"datasetId": 1004804,'+
                             '"datasetVersionId": 2473,'+
                             '"name": "Case Routing Model",'+
                             '"status": "SUCCEEDED",'+
                             '"progress": 1,'+
                             '"createdAt": "2017-06-22T19:39:38.000+0000",'+
                             '"updatedAt": "2017-06-22T19:43:39.000+0000",'+
                             '"learningRate": 0,'+
                             '"epochs": 300,'+
                             '"object": "training",'+
                             '"modelId": "5SXGNLCCOFGTMNMQYEOTAGBPVU",'+
                             '"trainParams": null,'+
                             '"trainStats": {'+
                             '"labels": 5,'+
                             '"examples": 150,'+
                             '"totalTime": "00:03:54:159",'+
                             '"trainingTime": "00:03:53:150",'+
                             '"earlyStopping": true,'+
                             '"lastEpochDone": 267,'+
                             '"modelSaveTime": "00:00:01:561",'+
                             '"testSplitSize": 11,'+
                             '"trainSplitSize": 139,'+
                             '"datasetLoadTime": "00:00:01:008"'+
                             '},"modelType": "text-intent"}');
            response.setStatusCode(200);
        }
        
         else if(request.getEndpoint().endsWith('intent')){
        response.setHeader('Content-Type', 'application/json');
            response.setBody('{"probabilities": ['+
                             '{"label": "Shipping Info",'+
                             '"probability": 0.82365495'+
                             '},{"label": "Sales Opportunity",'+
                             '"probability": 0.12523715'+
                             '},{"label": "Billing",'+
                             '"probability": 0.0487557'+
                             '},{'+
                             '"label": "Order Change",'+
                             '"probability": 0.0021365683'+
                             '},{"label": "Password Help",'+
                             '"probability": 0.0002156619'+
                             '}],"object": "predictresponse"}');
            response.setStatusCode(200);
        }       
        return response;  
    }
}